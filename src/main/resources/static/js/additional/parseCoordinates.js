const parseCoordinates = (coordinates) => {
    const separatedString = coordinates.split(' ');

    const coordinatesInteger = [];

    for(let i = 0; i < separatedString.length; i++) {
        coordinatesInteger.push(parseInt(separatedString[i]));
    }

    return coordinatesInteger;
};