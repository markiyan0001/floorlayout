let draw = (coordinates, color) => {

    let canvas = document.getElementById('myCanvas');
    let ctx = canvas.getContext('2d');

    ctx.clearRect(0,0,canvas.width,canvas.height);

    ctx.lineWidth = 3;
    ctx.strokeStyle = color;

    ctx.beginPath();

    ctx.moveTo(coordinates[0], coordinates[1]);

    for (let i = 2; i < coordinates.length; i += 2) {
        const x = coordinates[i];
        const y = coordinates[i + 1];
        ctx.lineTo(x, y);
    }

    ctx.lineTo(coordinates[0], coordinates[1]);

    ctx.stroke();
    ctx.closePath();

    location.href = '#bottom';
};