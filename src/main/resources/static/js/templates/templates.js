const roomFormTemplate = () => {
    return `
        <form id="roomForm">
            <input type="text" name="name" placeholder="Enter name of room... My Room"/><br>
            <input type="text" name="coordinates" placeholder="Enter coordinates... 10 20 30 40"/><br>
            <input type="text" name="color" placeholder="Enter color... yellow"/><br>
        </form>
        <button id="submitButton" type="button" onclick="createRoom()">Create</button>
    `;
};

const roomListTemplate = (list) => {

  let template = `
    <table id="roomTable">
        <tr>
            <th>NAME</th>
            <th>DATE</th>
            <th>COLOR</th>
            <th>COORDINATES</th>
            <th>DRAW</th>
            <th>DELETE</th>
        </tr>`;

  for(let i = 0; i < list.length; i++) {
      template += `<tr>`;
      template += `<td>${list[i].name}</td>`
      template += `<td>${list[i].dateOfCreation}</td>`
      template += `<td>${list[i].color}</td>`
      template += `<td>${list[i].coordinates}</td>`
      template += `<td><button id="draw" type="button" onclick="drawRoom(${list[i].roomId})">DRAW</button></td>`
      template += `<td><button id="delete" type="button" onclick="deleteRoom(${list[i].roomId})">DELETE</button></td>`
      template += `</tr>`;
  }

  template += '</table>';

  return template;
};