const roomFormRender = () => {
    document.getElementById('formDiv').insertAdjacentHTML('beforeend', roomFormTemplate());
};

const roomListRender = async (jsonList) => {
    await jsonList.then((data) => {
       document.getElementById('listDiv').insertAdjacentHTML('beforeend', roomListTemplate(data));
    });
};