const createRoom = async () => {

    const formData = new FormData(document.getElementById('roomForm'));

    const coordinates = parseCoordinates(formData.get('coordinates'));

    const room = {
        name: formData.get('name'),
        coordinates: coordinates,
        dateOfCreation: new Date(),
        color: formData.get('color')
    };

    const response = await fetch(`http://localhost:3000/room/`, {
        method: 'POST',
        body: JSON.stringify(room),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if(response.ok) {
        alert('Room is saved successfully!')
    } else {
        alert('Illegal coordinates!');
    }

};

const showRoomForm = () => {
    clearElements();
    roomFormRender();
};

const findAllRooms = async () => {
    clearElements();

    document.getElementById('myCanvas').getContext('2d').transform(1, 0, 0, -1, 0, 800);

    const response = await fetch(`http://localhost:3000/room/`, {
        method: 'GET'
    });

    await roomListRender(response.json());
};

const drawRoom = async (id) => {

    const room = await fetch(`http://localhost:3000/room/${id}`, {
        method: 'GET'
    });

    const roomJson = room.json();

    let coordinates = [];
    let color = '';

    await roomJson.then((data) => {
        coordinates = data.coordinates;
        color = data.color;
    });

    draw(coordinates, color);

};

const deleteRoom = async (id) => {
    document.getElementById("listDiv").innerHTML = "";

    await fetch(`http://localhost:3000/room/${id}`, {
        method: 'DELETE'
    });

    await findAllRooms();
};