package com.derevetskyi.markiyan.floorlayout.app.dao;

import com.derevetskyi.markiyan.floorlayout.app.model.Room;
import org.springframework.stereotype.Repository;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class FileRoomRepository implements RoomRepository {

    private List<Room> rooms;

    public FileRoomRepository() {
        rooms = loadData();
    }

    @Override
    public List<Room> findAll() {
        return rooms;
    }

    @Override
    public Room save (Room room) {
        room.setRoomId(fetchId());
        rooms.add(room);
        overwriteNewData();

        return room;
    }

    @Override
    public Optional<Room> findById(Long roomId) {
        return rooms.
                stream().filter(r -> r.getRoomId().equals(roomId)).findFirst();
    }

    @Override
    public void deleteById(Long roomId) {
        Room deleteRoom = findById(roomId).orElseThrow(() -> new RuntimeException("Room isn't found!"));
        rooms.remove(deleteRoom);
        overwriteNewData();
    }

    private List<Room> loadData() {
        ObjectInputStream objectInputStream = openObjectInputStream();
        List<Room> rooms = new ArrayList<>();
        Room room;

        try {
            while ((room = (Room) objectInputStream.readObject()) != null) {
                rooms.add(room);
            }
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return rooms;
    }

    private FileInputStream openInputStream() throws FileNotFoundException {
        return new FileInputStream(FileRoomRepository.class.getClassLoader().getResource("rooms.txt").getPath());
    }

    private FileOutputStream openOutputStream() throws FileNotFoundException {
        return new FileOutputStream(FileRoomRepository.class.getClassLoader().getResource("rooms.txt").getPath());
    }

    private ObjectInputStream openObjectInputStream() {
        ObjectInputStream objectInputStream = null;

        try {
            objectInputStream = new ObjectInputStream(openInputStream());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return objectInputStream;
    }

    private ObjectOutputStream openObjectOutputStream() {
        ObjectOutputStream objectOutputStream = null;

        try {
            objectOutputStream = new ObjectOutputStream(openOutputStream());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return objectOutputStream;
    }

    private void overwriteNewData() {
        ObjectOutputStream objectOutputStream = openObjectOutputStream();

        try {
            for(Room currentRoom : rooms) {
                objectOutputStream.writeObject(currentRoom);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private Long fetchId() {
        return rooms.get(rooms.size() - 1).getRoomId() + 1;
    }

}
