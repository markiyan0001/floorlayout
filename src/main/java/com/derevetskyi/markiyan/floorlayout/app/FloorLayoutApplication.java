package com.derevetskyi.markiyan.floorlayout.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.*;

@SpringBootApplication
public class FloorLayoutApplication {

    public static void main(String[] args) {
        SpringApplication.run(FloorLayoutApplication.class, args);
    }

}
