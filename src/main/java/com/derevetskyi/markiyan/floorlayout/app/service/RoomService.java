package com.derevetskyi.markiyan.floorlayout.app.service;

import com.derevetskyi.markiyan.floorlayout.app.dao.RoomRepository;
import com.derevetskyi.markiyan.floorlayout.app.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {

    @Autowired
    @Qualifier("fileRoomRepository")
    private RoomRepository roomRepository;

    public void saveRoom(Room room) {
        roomRepository.save(room);
    }

    public List<Room> findAll() {
        return roomRepository.findAll();
    }

    public Room findById(Long id) {
        return roomRepository.findById(id).get();
    }

    public void deleteById(Long roomId) {
        roomRepository.deleteById(roomId);
    }


}
