package com.derevetskyi.markiyan.floorlayout.app.model;

import javax.persistence.*;
import java.io.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "room")
public class Room implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_id")
    private Long roomId;

    @Column(name = "name")
    private String name;

    @Column(name = "date_of_creation")
    private LocalDate dateOfCreation;

    @Column(name = "color")
    private String color;

    @ElementCollection
    @CollectionTable(name = "room_coordinate", joinColumns = @JoinColumn(name = "room_id"))
    @Column(name = "coordinate")
    private List<Integer> coordinates;

    public Room() {

    }

    public Room(Long roomId, String name, String color, List<Integer> coordinates) {
        this.roomId = roomId;
        this.name = name;
        this.dateOfCreation = LocalDate.now();
        this.color = color;
        this.coordinates = coordinates;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Integer> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Integer> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomId=" + roomId +
                ", name='" + name + '\'' +
                ", dateOfCreation=" + dateOfCreation +
                ", color='" + color + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}
