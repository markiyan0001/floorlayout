package com.derevetskyi.markiyan.floorlayout.app.controller;

import com.derevetskyi.markiyan.floorlayout.app.model.Room;
import com.derevetskyi.markiyan.floorlayout.app.service.RoomService;
import com.derevetskyi.markiyan.floorlayout.app.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/room")
@CrossOrigin
public class RoomController {

    private RoomService roomService;

    private ValidationService validationService;

    public RoomController(@Autowired RoomService roomService,
                          @Autowired ValidationService validationService) {
        this.roomService = roomService;
        this.validationService = validationService;
    }

    @PostMapping("")
    public ResponseEntity<?> saveRoom(@RequestBody Room room) {
        if (validationService.checkCoordinates(room.getCoordinates())) {
            roomService.saveRoom(room);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    }

    @GetMapping("")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(roomService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{roomId}")
    public ResponseEntity<?> findById(@PathVariable("roomId") Long roomId) {
        return new ResponseEntity<>(roomService.findById(roomId), HttpStatus.OK);
    }

    @DeleteMapping("/{roomId}")
    public ResponseEntity<?> deleteById(@PathVariable("roomId") Long roomId) {
        roomService.deleteById(roomId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
