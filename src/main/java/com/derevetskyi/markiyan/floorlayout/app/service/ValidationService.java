package com.derevetskyi.markiyan.floorlayout.app.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ValidationService {

    public boolean checkCoordinates(List<Integer> coordinates) {
        return checkCount(coordinates) && !isDiagonal(coordinates) && !existsIntersectedLines(coordinates);
    }

    private boolean checkCount(List<Integer> coordinates) {
        return coordinates.size() >= 8;
    }

    private boolean isDiagonal(List<Integer> coordinates) {

        int length = coordinates.size();

        int i = 0;

        while(i + 3 <= length) {

            Integer currentX = coordinates.get(i);
            Integer currentY = coordinates.get(i + 1);

            Integer nextX = coordinates.get(i + 2);
            Integer nextY = coordinates.get(i + 3);

            if ((currentY - currentX) != (nextY - nextX)) return false;
            i += 2;
        }

        return true;
    }

    private boolean isTwoLinesIntersected(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
        double numeratorA, numeratorB;
        double Ua, Ub;

        int denominator = (y4 - y3) * (x1 - x2) - (x4 - x3) * (y1 - y2);

        if (denominator == 0) {
            return (x1 * y2 - x2 * y1) * (x4 - x3) - (x3 * y4 - x4 * y3) * (x2 - x1) == 0 && (x1 * y2 - x2 * y1) * (y4 - y3) - (x3 * y4 - x4 * y3) * (y2 - y1) == 0;
        } else {
            numeratorA = (x4 - x2) * (y4 - y3) - (x4 - x3) * (y4 - y2);
            numeratorB = (x1 - x2) * (y4 - y2) - (x4 - x2) * (y1 - y2);

            Ua = numeratorA / denominator;
            Ub = numeratorB / denominator;

            return Ua >= 0 && Ua <= 1 && Ub >= 0 && Ub <= 1;
        }
    }

    private boolean existsIntersectedLines(List<Integer> coordinates) {

        int length = coordinates.size();
        int i = 0;

        while ((i + 4) < length) {
            if (isTwoLinesIntersected(coordinates.get(i),
                    coordinates.get(i + 1), coordinates.get(i + 2),
                    coordinates.get(i + 3), coordinates.get(i + 4),
                    coordinates.get(i + 5), coordinates.get(i + 6), coordinates.get(i + 7))) {
                return true;
            }

            i += 4;
        }

        return false;
    }
}
