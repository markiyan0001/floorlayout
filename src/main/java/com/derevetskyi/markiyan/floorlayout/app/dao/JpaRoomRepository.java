package com.derevetskyi.markiyan.floorlayout.app.dao;

import com.derevetskyi.markiyan.floorlayout.app.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaRoomRepository extends JpaRepository<Room, Long>, RoomRepository{

}
