package com.derevetskyi.markiyan.floorlayout.app.dao;

import com.derevetskyi.markiyan.floorlayout.app.model.Room;

import java.util.List;
import java.util.Optional;

public interface RoomRepository {

    List<Room> findAll();

    Room save(Room room);

    Optional<Room> findById(Long roomId);

    void deleteById(Long id);

}
